==================================================================
https://keybase.io/galeksandrp
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://galeksandrp.gitlab.io
  * I am galeksandrp (https://keybase.io/galeksandrp) on keybase.
  * I have a public key with fingerprint 0B58 92A8 EE87 4BD9 C7E2  26EA 7546 5C70 4392 E07F

To do so, I am signing this object:

{
    "body": {
        "key": {
            "eldest_kid": "0101d69b5c138976dd96e98c32275ea95ad1b4f59ef9b113abf4abec18fbeb7391580a",
            "fingerprint": "0b5892a8ee874bd9c7e226ea75465c704392e07f",
            "host": "keybase.io",
            "key_id": "75465c704392e07f",
            "kid": "0101d69b5c138976dd96e98c32275ea95ad1b4f59ef9b113abf4abec18fbeb7391580a",
            "uid": "ea9c8b10855ef554d60d429685ffbf19",
            "username": "galeksandrp"
        },
        "service": {
            "hostname": "galeksandrp.gitlab.io",
            "protocol": "https:"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1495747374,
    "expire_in": 157680000,
    "prev": "a61358a24713536c6b5392bb9eca3af07a02b9f62f0326840ed6c2121a4bd4dd",
    "seqno": 30,
    "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owGtkl1IVEEUx3fXylIKUYzE6OEmhiFyP2buvbMRRUEvawZZ2oa0zNyZu97W9m53
7/pRSS9mXxSmL4VJUCGCFEVlECRtBFERayH2kh+VJUIUFGofEM21eqrH5uUwM///
b845czqWZvkC/tSWusTmzonD/ifTxBdeEwIHBWLTFiF4UIix+cAaKEu6kZhFhaAg
SqJEVUSgISk60lRKkcqQbiiyrEGGEcRUIsCEiJmISJKCiQkwYYakm4QRTUES1EUs
lAumFY8yJ+FYcdfDEqgjGeuM6RogFBkak2WVYQ0CFRqaCBQkM1EzubHeTnoOnhzB
SVZh2fyMbyLz6f1D/5/zTs3juMPQiSTqEDITQkBVkQIZqTo0TWJKyBMmmRPH+xhX
R3EDiyVxnDoJobVc4BeNlsG83nrF/C2qiFpuAya/aks4tmsbdgOX1LtuIhn0EG5L
wvM0MRL5TYsQK055T7mjkTlJy44LQYkrDdfy8BJAUAOaooFygTUnLIdFLE8BNVUX
+fLeYayRM7EqKVDHMtB4VFRDJZA3kxDEDKxgU9SwKBNkqrIpKrKqA5FR1ZAlWcL8
4wClglfg/rgtBBVOdXGUM5NWNI7dlMOE1pzj/pULfP6Ab9HCgDduvpwleX9mcG7T
Yt8sECsXFA5vo18fF4d3D74bfNZ+bPENnL2suGdX7UDVnbbhrubRTP6RjeMTj4bG
/I2p/pFX2Tfa5vKnNoU+pMvBVEGHf8XJvP2rO1ZmrmVGT0Wcmpoxc+jim2OZW6Wn
p7sefEY7i2Lv706OnV/V2VSLdlf11+wtO9QZfjhCS/Ltt33dl4Jr11X3DJYWzLaF
c8x0WV/tiafFkz9Cqdtf2g/cM358T9fNVAV6hbzcjyPVSzqXX9mRnhEvh0vEipvd
mddLK0Mbz1yd2NozWTCW+wwXbShKd31Tsprq1h9t/XQKfOk51x5LnQ2t2K5/Kny5
5+po77vHdK6w6vmLCwOB60Pj938C
=GVXY
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/galeksandrp

==================================================================
